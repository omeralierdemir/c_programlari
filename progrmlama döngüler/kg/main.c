#include <stdio.h>
#include <stdlib.h>

int main()
{
     int num,divisor=2,module,lastNum=0,kat=10;

    printf("Enter an integer : ");
    scanf("%d",&num);

    while(num>0)
    {
        module = num%divisor;
        printf("%d-%d\n",num,module);
        num = num/divisor;
        lastNum = lastNum + module*kat;
        kat = kat*10;
    }
    lastNum = lastNum /10;
    printf("%d",lastNum);
    return 0;
}
